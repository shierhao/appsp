package com.anji.sp.service;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.model.po.SpFilePO;

/**
 * 字典表
 *
 * @author Kean
 * @date 2020/06/23
 */
public interface SpFileService {
    /**
     * 查询角色列表
     *
     * @return
     */
    ResponseModel select(SpFilePO spFilePO);
    /**
     * 新增
     *
     * @pram spFilePO
     * @return
     */
    ResponseModel insert(SpFilePO spFilePO);
}