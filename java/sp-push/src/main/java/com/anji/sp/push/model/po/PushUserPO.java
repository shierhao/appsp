package com.anji.sp.push.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("push_user")
public class PushUserPO implements Serializable {

    private static final long serialVersionUID=1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 应用唯一key */
    private String appKey;

    /** 设备唯一标识 */
    private String deviceId;

    /** 厂商通道的token或者regId */
    private String manuToken;

    /** 设备类型：0:其他手机，1:华为，2:小米，3:oppo，4:vivo，5：ios */
    private String deviceType;

    /** 别名 */
    private String alias;

    /** 极光的用户id */
//    @TableField("registration_id")
    private String registrationId;

    /** 设备品牌（例如：小米6，iPhone 7plus） */
    private String brand;

    /** 系统版本例如：7.1.2  13.4.1 */
    private String osVersion;

    /** 0--已禁用 1--已启用  DIC_NAME=ENABLE_FLAG */
    private Integer enableFlag;

    /**  0--未删除 1--已删除 DIC_NAME=DEL_FLAG */
    private Integer deleteFlag;

    /** 创建者 */
    private Long createBy;

    /** 创建时间 */
    private LocalDateTime createDate;

    /** 更新者 */
    private Long updateBy;

    /** 更新时间 */
    private LocalDateTime updateDate;

    /** 备注信息 */
    private String remarks;


}
