package com.anji.sp.push.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @Author: Kean
 * @Date: 2021/2/20
 * @Description: jdk类HttpURLConnection调用第三方http接口
 * 通常分get和post两种方式
 */
public class HttpUrlConnectionToInterface {

    /**
     * 以post或get方式调用对方接口方法，
     * @param pathUrl
     * @param data
     */
    public static void doPostOrGet(String pathUrl, String data) {
        OutputStreamWriter out = null;
        BufferedReader br = null;
        String result = "";
        try {
            URL url = new URL(pathUrl);
            //打开和url之间的连接
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //请求方式
            conn.setRequestMethod("POST");//get
            //设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

            //DoOutput设置是否向httpUrlConnection输出，DoInput设置是否从httpUrlConnection读入，此外发送post请求必须设置这两个
            conn.setDoOutput(true);
            conn.setDoInput(true);

            //获取URLConnection对象对应的输出流
            out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            //发送请求参数即数据
            out.write(data);
            //flush输出流的缓冲
            out.flush();


            //构造一个字符流缓存
            //获取URLConnection对象对应的输入流
            InputStream is = conn.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            String str = "";
            while ((str = br.readLine()) != null){
                result += str;
            }
            System.out.println(result);
            //关闭流
            is.close();
            //断开连接，disconnect是在底层tcp socket链接空闲时才切断，如果正在被其他线程使用就不切断。
            conn.disconnect();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            //再次尝试关闭连接
            try {
                if (out != null){
                    out.close();
                }
                if (br != null){
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }


    public static void main(String[] args) {

        AJPushMessage ajPushMessage = new AJPushMessage();
        ajPushMessage.setAppKey("fb90cb5bc0c84a50883a3a2cc9295fbf");
        ajPushMessage.setContent("第三方调用测试1234");
        ajPushMessage.setTitle("第三方调用测试333323");
        ArrayList<String> deviceIds = new ArrayList<>();
        deviceIds.add("b8c67f0eb8b2b73b2c:5d:34:1e:6a:5b");
        ajPushMessage.setDeviceIds(deviceIds);
        HashMap<String, String> extras = new HashMap<>();
        extras.put("test", "1231241243");
        ajPushMessage.setExtras(extras);
        ajPushMessage.setPushType("1");
        HashMap<String, Object> sound = new HashMap<>();
        sound.put("sound", "aa");
        ajPushMessage.setAndroidConfig(sound);
        String data = JSONObject.toJSONString(ajPushMessage);

        doPostOrGet("http://open-appsp.anji-plus.com/sp/push/pushBatch", data);

    }

}
