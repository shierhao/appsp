package com.anji.sp.push.controller;

import com.alibaba.fastjson.JSONObject;
import com.anji.sp.aspect.PreSpAuthorize;
import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.constants.MqConstants;
import com.anji.sp.push.model.vo.PushHistoryVO;
import com.anji.sp.push.model.vo.PushMessageVO;
import com.anji.sp.push.model.vo.PushUserVO;
import com.anji.sp.push.model.vo.RequestSendBean;
import com.anji.sp.push.service.PushHistoryService;
import com.anji.sp.push.service.PushMessageService;
import com.anji.sp.push.service.PushUserService;
import com.anji.sp.push.service.impl.AJPushSendService;
import com.anji.sp.util.BeanUtils;
import com.anji.sp.util.SnowflakeUtils;
import com.anji.sp.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @Author: Kean
 * @Date: 2021/1/20
 * @Description:
 */
@RestController
@RequestMapping("/push")
@Api(tags = "推送对外接口")
public class PushController {

    @Autowired
    private AJPushSendService ajPushSendService;
    @Autowired
    private PushMessageService pushMessageService;

    @ApiOperation(value = "appSDK根据appKey推送初始化", httpMethod = "POST")
    @PostMapping("/init")
    public ResponseModel init(@RequestBody PushUserVO pushUserVO) {
        return ajPushSendService.init(pushUserVO);
    }

    @ApiOperation(value = "根据appKey批量推送", httpMethod = "POST")
    @RequestMapping("/pushBatch")
    public ResponseModel pushBatch(@RequestBody RequestSendBean requestSendBean) {
        requestSendBean.setSendOrigin("1");
        return ajPushSendService.pushBatch(requestSendBean);
    }

    @ApiOperation(value = "根据appKey全部推送", httpMethod = "POST")
    @RequestMapping("/pushAll")
    public ResponseModel pushAll(@RequestBody RequestSendBean requestSendBean) {
        requestSendBean.setSendOrigin("1");
        return ajPushSendService.pushAll(requestSendBean);
    }

    @ApiOperation(value = "根据msgId和appKey查询消息历史", httpMethod = "POST")
    @RequestMapping("/queryHistoryByAppKeyAndMsgId")
    public ResponseModel queryByMsgId(@RequestBody PushMessageVO requestModel) {
        return pushMessageService.queryHistoryMsgId(requestModel);
    }
}
