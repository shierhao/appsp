package com.anji.appsp;

import android.app.Application;
import com.anji.appsp.sdk.AppSpConfig;
import com.anji.appsp.sdk.AppSpLog;

public class AppContext extends Application {
    private static AppContext mInstance;
    //appkey是在Appsp创建应用生成的
    public static final String appKey = "860dc3bd5faa489bb7ab6d087f8ee6e5";

    public static AppContext getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //版本更新配置
        AppSpConfig.getInstance()
                .init(this, appKey)
                //可修改基础请求地址
                .setHost("https://openappsp.anji-plus.com")
                //正式环境可以禁止日志输出，通过Tag APP-SP过滤看日志
//                .setDebuggable(BuildConfig.DEBUG ? true : false)
                //务必要初始化，否则后面请求会报错
                .deviceInit();
    }
}
