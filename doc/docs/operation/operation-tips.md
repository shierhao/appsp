# 操作手册


1、登录成功后，进入首页后，点击“新增应用”图标，输入应用名，点击"确认"按钮，应用创建成功

![本地图片](../assets/createApplication.png)

![avatar](../assets/createApplication1.png)

## 版本管理

点击创建的应用，进入版本管理页面

### 新增安卓版本

1、点击“新增安卓版本”图标，进入版本新增页面
![avatar](../assets/version.png)

2、点击“点击上传”图标，上传需要更新的apk，输入更新日志
![avatar](../assets/version1.png)

3、版本配置，根据需求配置进行强制更新的历史版本和Android操作系统。例如历史版本选择了全部，Android操作系统选择了Android6.0，则只要是操作系统为Android6.0的设备都会强制更新该应用
![avatar](../assets/version2.png)

4、如果需要进行灰度发布，则点击按钮开启灰度发布，点击图标编辑发布率
![avatar](../assets/version3.png)

5、配置灰度发布率，需注意后一天的发布率值不能小于前一天，灰度发布时间最长为七天。
![avatar](../assets/version4.png)

6、点击“提交”按钮后，回到版本管理页面，新增的版本信息默认是下架的状态，即版本更新功能还未开启。此外，上架后可以对版本信息中的日志更新和版本配置进行编辑
![avatar](../assets/version5.png)

点击创建的应用，进入版本管理页面

### 新增ios版本
新增ios版本操作流程与新增安卓版本类似，不同的地方是ios的下载地址和版本名，版本信息需要手动输入
![avatar](../assets/version6.png)

## 公告管理

1、点击“新增公告”图标，进入公告管理页面
![avatar](../assets/notice.png)

2、填写新增公告的相关信息
![avatar](../assets/notice1.png)

3、公告创建成功后默认状态为未开启，且可以对公告进行查看、编辑、删除等操作
![avatar](../assets/notice2.png)


## 成员管理

1、点击“添加成员”图标，弹出成员管理窗口
![avatar](../assets/member.png)

2、选择成员和该成员的角色，点击“确认”按钮，成员添加成功
![avatar](../assets/member1.png)

3、成员添加成功后可以进行编辑职位和删除成员操作
![avatar](../assets/member2.png)

## 账号管理

1、点击“账号管理”进入账号管理页面，可以通过用户名或账号查询用户信息，点击“新建用户”图标，弹出新建用户窗口
![avatar](../assets/account.png)

1、在弹窗中输入账号和用户名，点击“确认”，用户创建成功
![avatar](../assets/account1.png)

## 基础设置

点击`添加`进入基础设置页面，基础设置中可以对Android版本、ios版本、公告名称、角色配置进行添加
![avatar](../assets/basis.png)

### Android版本添加
点击`添加`按钮，弹出Android版本弹窗，在弹窗中输入Android版本，点击确认按钮，Android版本添加成功，添加成功后不可删除
![avatar](../assets/basis1.png)

### iOS版本添加
点击`添加`按钮，弹出ios版本弹窗，在弹窗中输入ios版本，点击确认按钮，ios版本添加成功，添加成功后不可删除
![avatar](../assets/basis2.png)

### 公告添加
点击`添加`按钮，弹出公告名称弹窗，在弹窗中输入公告信息，点击确认按钮，公告名称添加成功，添加成功后不可删除将鼠标放在公告名称上可以显示公告的标识，公告标识是唯一的，是产品和开发约定的标识，在SDK体现
![avatar](../assets/basis3.png)

### 角色添加
点击`添加`按钮，弹出角色配置弹窗，在弹窗中输入角色信息，点击确认按钮，角色配置添加成功
![avatar](../assets/basis4.png)

### 角色编辑
点击角色，弹出角色配置弹窗，在弹窗中输入角色信息，点击确认按钮，角色配置添加成功
![avatar](../assets/basis4.png)