# 推送-华为   

## 官网配置

1、	登录华为开发者平台，点击“管理中心”->“AppGallery Connect” ，进入“AppGallery Connect”中。   
2、 点击“我的项目”->“添加项目”->输入项目名称“PushTest”->“确认”，如下图所示：
![avatar](../assets/hw1.png)  
3、	添加应用，点击“添加应用”，如下图所示：  
![avatar](../assets/hw2.png) 
4、 在添加引用界面填写应用信息，点击“确认”，如下图所示：    
![avatar](../assets/hw3.png)  
5、	下载配置文件agconnect-services.json，如下图所示。  
![avatar](../assets/hw4.png)  
8、	设置“数据存储位置”，点击![avatar](../assets/hw6.png)  ，选着数据存储位置，如下图所示：  
![avatar](../assets/hw7.png)  
9、	配置“SHA256证书指纹”，以Windows系统为例：  
```
A、使用CMD命令打开命令行工具，执行cd命令进入keytool.exe所在的目录（以下样例为JDK安装在C盘的Program Files目录）
    C:\>cd C:\Program Files\Java\jdk\bin
    C:\Program Files\Java\jdk\bin>

B、执行命令keytool -list -v -keystore <keystore-file>，按命令行提示进行操作。<keystore-file>为应用签名文件的完整路径
   keytool -list -v -keystore C:\TestApp.jks

C、根据结果获取对应的SHA256指纹
```  
10、打开相关服务，点击“API管理”，打开“Push Kit”，如下图所示:  
![avatar](../assets/hw8.png)  
11、在左侧导航栏选择“增长 > 推送服务”，点击“立即开通”，在弹出的提示框中点击“确定”  
![avatar](../assets/hw9.png)  
12、确认开通后，您可以在“配置”页签开通和关闭您的项目级和应用级的推送服务权益  
![avatar](../assets/hw10.png)   

## 推送测试 
 
1、点击“推送服务”->“添加推送通知”，如下图所示：  
![avatar](../assets/hw11.png)   
2、添加推送消息内容，如下图所示：  
![avatar](../assets/hw12.png)  



